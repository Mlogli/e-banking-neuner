<?php
require_once 'Database.php';
require_once "DatabaseObject.php";
require_once "UserModel.php";


class TransactionModel implements DatabaseObject
{
    private $transaction_id;
    private $zieliban;
    private $zielbic;
    private $betrag;
    private $verwendungszweck;
    private $zahlungsreferenz;
    private $datum;
    private $user_user_id;

    private $errors = [];


    public function __construct()
    {

    }



    public static function getAllUberweisungen($id, $iban){

         $pdo = Database::connect();
            $stmt = $pdo->prepare("SELECT * FROM transaction WHERE zieliban = ? OR user_user_id = ?");
            $stmt->execute([$iban,$id]);
            $transArray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
           Database::disconnect();
            return $transArray;

    }

    public function addTransaction(){
            //erstelle Transaction
            $this->create();
            $pdo = Database::connect();


            //update Kontostand versender
            $user = UserModel::get($this->user_user_id);
            $user->setKontostand($user->getKontostand()-$this->betrag);
            $user->update();


            //update empfänger
            $empfaengerUser = UserModel::getByIban($this->zieliban);
            $empfaengerUser->setKontostand($empfaengerUser->getKontostand()+$this->betrag);
            $empfaengerUser->update();

            Database::disconnect();

    }

    public static function searchText($filter){
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction WHERE verwendungszweck LIKE ? OR zahlungsreferenz LIKE ?");
        $stmt->execute([$filter,$filter]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;

    }

    public static function searchDate($firstDate, $secondDate){
        $firstDateConv = $firstDate.' 00:00:00';
        $secondDateConv = $secondDate.' 23:59:59';

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction  WHERE datum BETWEEN ? AND ?");
        $stmt->execute([$firstDateConv, $secondDateConv]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;
    }

    public static function searchValue($firstValue, $secondValue){
        $firstValue = str_replace(",",".", $firstValue);
        $secondValue = str_replace(",",".", $secondValue);

        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM transaction  WHERE betrag BETWEEN ? AND ?");
        $stmt->execute([$firstValue, $secondValue]);
        $transarray = $stmt->fetchAll(PDO::FETCH_CLASS, 'TransactionModel');
        Database::disconnect();
        return $transarray;
    }

    public function auszahlen($iban){
        //erstelle Transaction
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM user WHERE iban = ?");
        $stmt->execute([$iban]);
        $usertemp = $stmt->fetchObject("UserModel");
        $userid = $usertemp->getUserId();
        $this->createAuszahlung($userid);

        //update Kontostand auszahlen
        $user = UserModel::get($userid);
        $user->setKontostand($user->getKontostand()-$this->betrag);
        $user->update();

        Database::disconnect();
    }

    public function einzahlen(){
        //erstelle Transaction
        $this->create();
        $pdo = Database::connect();


        //update empfänger einzahlen
        $empfaengerUser = UserModel::getByIban($this->zieliban);
        $empfaengerUser->setKontostand($empfaengerUser->getKontostand()+$this->betrag);
        $empfaengerUser->update();

        Database::disconnect();
    }





    /**
     * @return mixed
     */
    public function getTransactionId()
    {
        return $this->transaction_id;
    }

    /**
     * @param mixed $transaction_id
     */
    public function setTransactionId($transaction_id)
    {
        $this->transaction_id = $transaction_id;
    }

    /**
     * @return mixed
     */
    public function getZieliban()
    {
        return $this->zieliban;
    }

    /**
     * @param mixed $zieliban
     */
    public function setZieliban($zieliban)
    {
        $this->zieliban = $zieliban;
    }

    /**
     * @return mixed
     */
    public function getZielbic()
    {
        return $this->zielbic;
    }

    /**
     * @param mixed $zielbic
     */
    public function setZielbic($zielbic)
    {
        $this->zielbic = $zielbic;
    }

    /**
     * @return mixed
     */
    public function getBetrag()
    {
        return $this->betrag;
    }

    /**
     * @param mixed $betrag
     */
    public function setBetrag($betrag)
    {
        $this->betrag = $betrag;
    }

    /**
     * @return mixed
     */
    public function getVerwendungszweck()
    {
        return $this->verwendungszweck;
    }

    /**
     * @param mixed $verwendungszweck
     */
    public function setVerwendungszweck($verwendungszweck)
    {
        $this->verwendungszweck = $verwendungszweck;
    }

    /**
     * @return mixed
     */
    public function getZahlungsreferenz()
    {
        return $this->zahlungsreferenz;
    }

    /**
     * @param mixed $zahlungsreferenz
     */
    public function setZahlungsreferenz($zahlungsreferenz)
    {
        $this->zahlungsreferenz = $zahlungsreferenz;
    }

    /**
     * @return mixed
     */
    public function getDatum()
    {
        return $this->datum;
    }

    /**
     * @param mixed $datum
     */
    public function setDatum($datum)
    {
        $this->datum = $datum;
    }

    /**
     * @return mixed
     */
    public function getUserUserId()
    {
        return $this->user_user_id;
    }

    /**
     * @param mixed $user_user_id
     */
    public function setUserUserId($user_user_id)
    {
        $this->user_user_id = $user_user_id;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


    public function create()
    {
        $pdo = Database::connect();
        $sql = "INSERT INTO transaction (zieliban, betrag, verwendungszweck, zahlungsreferenz, user_user_id) VALUES (?,?,?,?,?)";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$this->zieliban, $this->betrag, $this->verwendungszweck, $this->zahlungsreferenz, $this->user_user_id]);
        Database::disconnect();
    }

    public function createAuszahlung($userid)
    {
        $pdo = Database::connect();
        $sql = "INSERT INTO transaction (zieliban, betrag, verwendungszweck, zahlungsreferenz, user_user_id) VALUES (?,?,?,?,?)";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$this->zieliban, $this->betrag, $this->verwendungszweck, $this->zahlungsreferenz, $userid]);
        Database::disconnect();
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public static function get($id)
    {
        // TODO: Implement get() method.
    }

    public static function getAll()
    {
        // TODO: Implement getAll() method.
    }
}
