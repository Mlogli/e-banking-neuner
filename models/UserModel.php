<?php
require_once "DatabaseObject.php";
require_once 'Database.php';
class UserModel implements DatabaseObject
{
    private $user_id = '';
    private $username = '';
    private $password ='';
    private $iban = '';
    private $bic = '';
    private $kontostand = '';
    private $isAngestellter;

    private $errors = [];


    public function __construct()
    {

    }

    public static function getUserByNameAndPasswort($username, $password){
            $pdo = Database::connect();
            $stmt = $pdo->prepare("SELECT * FROM user WHERE username = ? AND password = ?");
            $stmt->execute(array($username,$password));
            $data = $stmt->fetchObject('UserModel');
            Database::disconnect();
            return $data !== false ? $data : null;
    }

    public function login(){
        $u = self::getUserByNameAndPasswort($this->username, $this->password);
        if ($u != null){
            $_SESSION['email'] = $this->email;
            return true;
        }
        else{
            unset($_SESSION['email']);
            print_r("ES GIBT FALSE");
            return false;
        }

    }


    public static function logout()
    {
        unset($_SESSION['email']);
        session_destroy();
    }

    public static function isLoggedIn(){
        return isset($_SESSION['email']) && strln($_SESSION['email']) > 0;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getIsAngestellter()
    {
        return $this->isAngestellter;
    }

    /**
     * @param mixed $isAngestellter
     */
    public function setIsAngestellter($isAngestellter)
    {
        $this->isAngestellter = $isAngestellter;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    }

    /**
     * @return string
     */
    public function getKontostand()
    {
        return $this->kontostand;
    }

    /**
     * @param string $kontostand
     */
    public function setKontostand($kontostand)
    {
        $this->kontostand = $kontostand;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }


    public function create()
    {
            $pdo = Database::connect();
            $sql = "INSERT INTO user (username, password, iban, bic, kontostand, isAngestellter) VALUES (?,?,?,?,?,?)";
            $stmt= $pdo->prepare($sql);
            $stmt->execute([$this->username, $this->password, $this->iban, $this->bic, $this->kontostand, $this->isAngestellter]);
            Database::disconnect();

    }

    public function update()
    {
        $pdo = Database::connect();
        $sql = "UPDATE user set username = ?, password = ?, iban = ?, bic = ?, kontostand = ?, isAngestellter = ? WHERE user_id = ?";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$this->username, $this->password, $this->iban, $this->bic, $this->kontostand, $this->isAngestellter, $this->user_id]);
        Database::disconnect();
    }

    public static function getAll()
    {
        // TODO: Implement getAll() method.
    }

    public static function get($id)
    {
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM user WHERE user_id = ?");
        $stmt->execute(array($id));
        $data = $stmt->fetchObject('UserModel');
        Database::disconnect();
        return $data !== false ? $data : null;
    }

    public static function getByIban($iban)
    {
        $pdo = Database::connect();
        $stmt = $pdo->prepare("SELECT * FROM user WHERE iban = ?");
        $stmt->execute(array($iban));
        $data = $stmt->fetchObject('UserModel');
        Database::disconnect();
        return $data !== false ? $data : null;
    }
}
