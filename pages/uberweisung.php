<?php
session_start();
require "../sql/database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";
$userdata = $_SESSION['user'];
$user = unserialize($userdata);




if(isset($_POST['ueberweisen'])) {
    if(UserModel::getByIban($_POST['iban']) != null){
        $transaction = new TransactionModel();
        $transaction->setZieliban($_POST['iban']);
        $transaction->setVerwendungszweck($_POST['verwendungszweck']);
        $transaction->setZahlungsreferenz($_POST['zahlungsreferenz']);
        $transaction->setBetrag(str_replace(',', '.', $_POST['betrag']));
        $transaction->setUserUserId($user->getUserId());

        $transaction->addTransaction();

        $user->setKontostand($user->getKontostand()-$transaction->getBetrag());
        $_SESSION['user'] = serialize($user);
    }
    else{
        echo "<div class=\"alert  alert-danger\">
<h3 class=\"alert-heading\">;(</h3>
<p class=\"mb-0\">Denn IBAN gibs nit.</p>
</div>";
    }

}

?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="">LL-Bank Neue Überweisung</a>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="userview.php">Zurück</a>
                </li>
            </ul>
        </div>
        <button onclick="window.location='logout.php';" type="button" class="btn btn-default">Abmelden</button>
    </div>

</div>

<div class="container">
    <div class="space70"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- $_Server um die Userdaten direkt aus der Datenbank überprüfen zu können-->
            <form action="uberweisung.php" method="post">
                <fieldset>
                    <center><h2>Neuen Überweisung</h2></center>
                    <div class="space40"></div>
                    <div class="form-group">
                        <label>IBAN-Empfänger:</label>
                        <input class="form-control" placeholder="AT34 23223 22332" name="iban" type="text">
                    </div>
                    <div class="form-group">
                        <label>Verwendungszeck:</label>
                        <input class="form-control" name="verwendungszweck" type="text">
                    </div>
                    <div class="form-group">
                        <label>Zahlungsreferenz:</label>
                        <input class="form-control" name="zahlungsreferenz" type="text">
                    </div>
                    <div class="form-group">
                        <label>Betrag:</label>
                        <input class="form-control" placeholder="00,00" name="betrag" type="number" step="0.01" min="0">
                    </div>
                    <p></p>
                    <div>
                        <button name="ueberweisen" type="submit" class="btn btn-primary">Überweisen</button>
                    </div>
                </fieldset>

            </form>
        </div>
    </div>

</div>
</body>

</html>