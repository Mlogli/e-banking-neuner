<?php
session_start();
require "../sql/database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";

$userdata = $_SESSION['user'];
$user = unserialize($userdata);
$transactions=[];

if(isset($_POST['suchen'])){
    $text = $_POST['text'];
    $transactions = TransactionModel::searchText($text);
    $transactions = array_reverse($transactions);
}




?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="userview.php">LL-Bank</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="uberweisung.php">Neue Überweisung</a>
            </li>

        </ul>
    </div>
    <button onclick="window.location='logout.php';" type="button" class="btn btn-default">Abmelden</button>

</div>
<!--End Navbar -->


<ul class="list-group ">
    <li class="list-group-item text-center list-group-item-info ">
        <p><?=$user->getUsername()?><br>
        <h2>€ <?=$user->getKontostand()?></h2><br>
        Kontonummer: <?= $user->getIban()?>
        </p>
    </li>
</ul>

<div class="navbar navbar-expand-lg navbar-dark bg-primary ">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="">Suche </a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="userviewTextSearch.php">Text</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="userviewDateSearch.php">Datum</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="userviewValueSearch.php">Betrag</a>
            </li>
        </ul>
    </div>

</div>

<form action="userviewTextSearch.php" method="post">
    <div class="col-sm-12">
        <label>
            Von:
            <input type="text" name="text" value="">
        </label>
        <button name="suchen" type="submit" class="btn-primary">Suchen</button>
    </div>

</form>
<?php
if(isset($_POST['suchen'])){
    if(empty($transactions)){
        echo "<div class=\"alert  alert-danger\">
<h3 class=\"alert-heading\">;(</h3>
<p class=\"mb-0\">Nichts gefunden.</p>
</div>";
    }
}?>


<?php
?>
<ul class="list-group">
    <?php
    foreach ($transactions as $val) {
        if($val->getUserUserId() == $user->getUserId()){
            ?>

            <li class="list-group-item list-group-item-danger">
                <p><h3> Ausgang: -€ <?=$val->getBetrag()?></h3>
                Verwendungszweck: <?=$val->getVerwendungszweck()?><br>
                Zahlungsreferenz: <?=$val->getZahlungsreferenz()?><br>
                Datum: <?=$val->getDatum()?></p>
            </li>

            <?php
        }
        else {
            ?>

            <li class="list-group-item list-group-item-success">
                <p><h3>Eingang: +€ <?=$val->getBetrag()?></h3>
                Verwendungszweck: <?=$val->getVerwendungszweck()?><br>
                Zahlungsreferenz: <?=$val->getZahlungsreferenz()?><br>
                Datum: <?=$val->getDatum()?></p>
            </li>
            <?php
        }
    }
    ?>
</ul>


</div>
</body>

</html>