<?php
session_start();
require "../sql/database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";
$userdata = $_SESSION['user'];
$user = unserialize($userdata);




if(isset($_POST['ueberweisen'])) {
if(UserModel::getByIban($_POST['iban']) != null) {
    $transaction = new TransactionModel();

    $transaction->setZieliban($_POST['iban']);
    $transaction->setVerwendungszweck($_POST['verwendungszweck']);
    //$transaction->setZahlungsreferenz($_POST['zahlungsreferenz']);
    $transaction->setBetrag(str_replace(',', '.', $_POST['betrag']));
    $transaction->setUserUserId($user->getUserId());


    if ($_POST['verwendungszweck'] == "Einzahlung") {
        $transaction->einzahlen();
    } else {
        $transaction->auszahlen($_POST['iban']);
    }

    $user->setKontostand($user->getKontostand() - $transaction->getBetrag());
    $_SESSION['user'] = serialize($user);
}
else{

    echo "<div class=\"alert  alert-danger\">
<h3 class=\"alert-heading\">;(</h3>
<p class=\"mb-0\">Denn IBAN gibs nit.</p>
</div>";
}
}

?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">



</head>
<body>
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="">LL-Bank Neue Einzahlung/Auszahlung</a>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="angestellterview.php">Zurück</a>
                </li>
            </ul>
        </div>
        <button onclick="window.location='logout.php';" type="button" class="btn btn-default">Abmelden</button>
    </div>

</div>

<div class="container">
    <div class="space70"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- $_Server um die Userdaten direkt aus der Datenbank überprüfen zu können-->
            <form action="einzahlungAuszahlung.php" method="post">
                <fieldset>
                    <center><h2>Neue Einzahlung/Auszahlung</h2></center>
                    <div class="space40"></div>
                    <div class="form-group">
                        <label>IBAN-Empfänger:</label>
                        <input class="form-control" placeholder="AT34 23223 22332" name="iban" type="text">
                    </div>
                    <div class="form-group">
                        <label>Betrag:</label>
                        <input class="form-control" placeholder="00,00" name="betrag" type="number" step="0.01" min="0">
                    </div>
                    <div class="form-group">
                        <label for="sel1">Auswählen:</label>
                        <select name="verwendungszweck" class="form-control" id="sel1">
                            <option name="verwendungszweck">Einzahlung</option>
                            <option name="verwendungszweck">Auszahlung</option>
                        </select>
                    </div>
                    <p></p>
                    <div>
                        <button onclick="display()" name="ueberweisen" type="submit" class="btn btn-primary">Bestätigen und Drucken</button>
                        <script>
                            function display() {
                                window.print();
                            }
                        </script>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>
</body>

</html>