<?php
session_start();
require "../sql/database.php";
require "../models/UserModel.php";
require "../models/TransactionModel.php";

$userdata = $_SESSION['user'];
$user = unserialize($userdata);
$transactions = TransactionModel::getAllUberweisungen($user->getUserId(), $user->getIban());
$transactions = array_reverse($transactions);


?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid text-center">
        <a class="navbar-brand" href="userview.php">LL-Bank</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="einzahlungAuszahlung.php">Einzahlung/Auszahlung</a>
            </li>

        </ul>
    </div>
    <button onclick="window.location='logout.php';" type="button" class="btn btn-default">Abmelden</button>

</div>
<!--End Navbar -->


<ul class="list-group ">
    <li class="list-group-item text-center list-group-item-info ">
        <p>Wilkommen<br>
        <h2>Mitarbeiter <?=$user->getUsername()?></h2><br>
        </p>
    </li>
</ul>

<?php
?>
</div>
</body>

</html>
