<?php
session_start();
require "../sql/database.php";
require "../models/UserModel.php";



if(isset($_POST['login'])) {
    $user = new UserModel();
    $user->setUsername($_POST['username']);
    $user->setPassword($_POST['password']);
    $user->setIban($_POST['iban']);
    $user->setBic($_POST['bic']);
    $user->setKontostand(str_replace(',', '.', $_POST['kontostand']));
    $user->setIsAngestellter(false);
    $user->create();
}

if(isset($_POST['back'])) {
    header("Location: ../index.php");
    exit();
}

?>
<head>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" href="../css/bootstrap-grid.min.css">
</head>
<body>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="">LL-Bank</a>
    </div>
</div>
<!--End Navbar -->

<div class="container">
    <div class="space70"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- $_Server um die Userdaten direkt aus der Datenbank überprüfen zu können-->
            <form action="register.php" method="post">
                <fieldset>
                    <center><h2>Neuen Benutzer registrieren</h2></center>
                    <div class="space40"></div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username:</label>
                        <input class="form-control" placeholder="Max Mustermann" name="username" type="text">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Passwort:</label>
                        <input class="form-control" placeholder="*****" name="password" type="password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">IBAN:</label>
                        <input class="form-control" placeholder="5555" name="iban" type="text">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">BIC:</label>
                        <input class="form-control" placeholder="SPIHATXXX" name="bic" type="text">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Kontostand:</label>
                        <input class="form-control" placeholder="00,00" name="kontostand" type="text">
                    </div>
                    <p></p>
                    <div>
                        <button name="login" type="submit" class="btn btn-primary">Registrieren</button>
                        <button name="back" type="submit" class="btn btn-primary">Zurück</button>

                    </div>
                </fieldset>
            </form>
        </div>
    </div>

</div>
</body>

</html>