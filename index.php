<?php
session_start();
require 'sql/database.php';
require 'models/UserModel.php';

if(isset($_SESSION['user'])){
    $user = unserialize($_SESSION['user']);
    if($user->getIsAngestellter()){
        header("Location: ./pages/angestellterview.php");
        exit();
    }
    else {
        header("Location: ./pages/userview.php");
        exit();
    }

}

$error = "";

if(isset($_POST['register'])) {
    header("Location: ./pages/register.php");
    exit();
}


if(isset($_POST['login'])) {



    $username = $_POST['username'];
    $password = $_POST['password'];

    $user = UserModel::getUserByNameAndPasswort($username,$password);

    if ($user != null){
        if ($user->getIsAngestellter()){
            $_SESSION['user'] = serialize($user);
            header("Location: ./pages/angestellterview.php");
            exit();
        }
        else{
            $_SESSION['user'] = serialize($user);
            header("Location: ./pages/userview.php");
            exit();
        }

    }else $error = '<div class="alert  alert-danger">
        <h3 class="alert-heading">ERROR!</h3>
        <p class="mb-0">Die eingegeben Daten sind falsch.</p>
    </div>';
}
?>
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-grid.css">
    <link rel="stylesheet" href="css/bootstrap-grid.min.css">
</head>
<body>
<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="">LL-Bank</a>
    </div>
</div>
<!--End Navbar -->

<div class="container">
    <div class="space70"></div>
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <!-- $_Server um die Userdaten direkt aus der Datenbank überprüfen zu können-->
            <form action="index.php" method="POST">
                <fieldset>
                    <center><h2>Anmeldung</h2></center>
                    <div class="space40"></div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username:</label>
                        <input class="form-control" placeholder="Max Mustermann" name="username" type="text">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Passwort:</label>
                        <input class="form-control" placeholder="*****" name="password" type="password">
                    </div>
                    <p></p>
                    <div>
                        <button name="login" type="submit" class="btn btn-primary">Anmelden</button>
                    </div>

                </fieldset>
            </form>
            <form action="pages/register.php" method="POST"><button name="register" type="submit" class="btn btn-primary">Registrieren</button></form>
            <?php echo $error; ?>
        </div>
    </div>

</div>
</body>

</html>